# Charisma Backend API

## Entities

### Push Subscription

> `id` is a combination of `device_id` and `host` joined with `|` (`device_id|host`) and encoded with `base64url` (without padding)

| Attribute   | Type                        |
| ----------- | --------------------------- |
| `id`        | String                      |
| `device_id` | String                      |
| `platform`  | [String (Enum)](#platform)  |
| `host`      | String                      |
| `data`      | [Alerts Hash](#alerts-hash) |

### Platform

- `apns` - Apple iOS

### Alerts Hash

| Attribute   | Type    |
| ----------- | ------- |
| `follow`    | Boolean |
| `favourite` | Boolean |
| `reblog`    | Boolean |
| `mention`   | Boolean |

## REST

### Push Subscriptions

- Response format: JSON
- Requires `Authorization` header with value `Bearer PLEROMA_TOKEN`

#### POST /api/v1/push_subscriptions

Creates a new push subscription.

> Each access token can have one push subscription. If you create a new subscription, the old subscription is deleted.

Returns: [Push Subscription](#Push-Subscription)

##### Parameters

| Name                      | Description                                                          | Required |
| ------------------------- | -------------------------------------------------------------------- | -------- |
| `device_id`               | A device ID.                                                         | Required |
| `platform`                | Platform. Currently, only `apns` (iOS) is supported.                 | Required |
| `host`                    | A Pleroma instance host, must include URL scheme.                    | Required |
| `data[alerts][follow]`    | Boolean of whether you want to receive follow notification event.    | Optional |
| `data[alerts][favourite]` | Boolean of whether you want to receive favourite notification event. | Optional |
| `data[alerts][reblog]`    | Boolean of whether you want to receive reblog notification event.    | Optional |
| `data[alerts][mention]`   | Boolean of whether you want to receive mention notification event.   | Optional |

#### GET /api/v1/push_subscriptions/:id

Returns: [Push Subscription](#Push-Subscription)

#### PUT /api/v1/push_subscriptions/:id

Updates a push subscription. Only the `data` part can be updated, e.g. which types of notifications are desired. To change fundamentals, a new subscription must be created instead.

Returns: [Push Subscription](#Push-Subscription)

##### Parameters

| Name                      | Description                                                          | Required |
| ------------------------- | -------------------------------------------------------------------- | -------- |
| `data[alerts][follow]`    | Boolean of whether you want to receive follow notification event.    | Optional |
| `data[alerts][favourite]` | Boolean of whether you want to receive favourite notification event. | Optional |
| `data[alerts][reblog]`    | Boolean of whether you want to receive reblog notification event.    | Optional |
| `data[alerts][mention]`   | Boolean of whether you want to receive mention notification event.   | Optional |


#### DELETE /api/v1/push_subscriptions/:id

Remove a push subscription.

Returns: empty response body
