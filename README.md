# Charisma (Work In Progress)

- [API Documentation](API.md)
- At the moment the only feature is the native push to iOS/Android devices
- Internally it subscribes for web push notifications on the target pleroma instance and then forwards them to mobile devices via native push (similar to [Mastodon](https://docs.joinmastodon.org/api/push/)).
- Testing instance: <https://roma.ugroza.ru/>

## Run

### Requrements

- PostgreSQL v9.5+

### Environment Variables

- `SECRET_KEY_BASE` — a secret key used as a base to generate secrets for encrypting and signing data.
- `PORT` — a port to listen. The server URL port is expected to be `443`.
- `HOSTNAME` - a domain name of the server.
- `LOGGER_LEVEL`  —  Optional, default: `info`.

#### APNS

- `APNS_KEY` — A string with a key or path to a key file.
- `APNS_KEY_IDENTIFIER`
- `APNS_TEAM_ID`
- `APNS_MODE` — Optional. Possible values: `dev` or `prod`, default: `prod`.
- `APNS_TOPIC` — Optional.

#### Database

- `DATABASE_URL`

or

- `DATABASE_USER`
- `DATABASE_PASS`
- `DATABASE_NAME`
- `DATABASE_HOST`

### OTP Releases

- Build: `MIX_ENV=prod mix release --env=prod`
- Run: `_build/prod/rel/charisma/bin/charisma foreground`
- Run migrations: `_build/prod/rel/charisma/bin/charisma migrate`
- Config file: `_build/prod/rel/charisma/etc/config.exs`

## Notification Payload

### APNS Example

```json
{
  "access_token": "YvRqZatg9c5180GxZL7nZ132jP-A3mKPQjY9uDs4t-A",
  "aps": {
    "alert": {"body": "@test: @user hello!", "title": "New Mention"}
  },
  "host": "https://pleroma.ugroza.ru",
  "icon": "https://pleroma.ugroza.ru/images/avi.png",
  "notification_id": 6,
  "notification_type": "mention",
  "pleroma": {"activity_id": "9gzdzzmJUyAfebXqi0"},
  "preferred_locale": "en"
}
```

### FCM Example

```json
{
  "notification": {"body": "@test: @egor 11"},
  "data": {
    "access_token": "WFiadabq18KUIFFm4o-D_9_tAiBy9hzyFIDJQCwwPvw",
    "host": "https://pleroma.ugroza.ru",
    "icon": "https://pleroma.ugroza.ru/images/avi.png",
    "notification_id": 160,
    "notification_type": "mention",
    "pleroma": {"activity_id": "9hBsUYRWO3yvxVmGUi"},
    "preferred_locale": "en",
    "title": "New Mention"
  }
}
```
