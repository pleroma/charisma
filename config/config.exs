# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :charisma,
  ecto_repos: [Charisma.Repo]

# Configures the endpoint
config :charisma, CharismaWeb.Endpoint,
  # url: [host: "localhost"],
  secret_key_base: "b9Yg9/xMX8HdYaPVBL2x8I9coqzlQtOunZL0kqUtM/6869Z+ZsgqoUL8Ktd7f8A3",
  render_errors: [view: CharismaWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Charisma.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :tesla, adapter: Tesla.Adapter.Hackney

config :pleroma_job_queue, :queues,
  apns: 200,
  fcm: 200

config :pigeon, :apns,
  apns_default: %{
    # priv/keys/AuthKey.p8
    key: {:charisma, "keys/AuthKey.p8"},
    key_identifier: "ABC1234567",
    team_id: "DEF8901234",
    mode: :dev
  }

config :pigeon, :fcm,
  fcm_default: %{
    key: "your_fcm_key_here"
  }

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
