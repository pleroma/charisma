use Mix.Config

config :charisma, CharismaWeb.Endpoint,
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  http: [:inet6, port: System.get_env("PORT")],
  url: [host: System.get_env("HOSTNAME"), scheme: "https", port: 443],
  server: true

if System.get_env("LOGGER_LEVEL") do
  config :logger,
    level: String.to_atom(System.get_env("LOGGER_LEVEL"))
else
  # Do not print debug messages in production
  config :logger, level: :info
end

config :charisma, Charisma.Repo, pool_size: 15

if System.get_env("DATABASE_URL") do
  config :charisma, Charisma.Repo, url: System.get_env("DATABASE_URL")
else
  config :charisma, Charisma.Repo,
    username: System.get_env("DATABASE_USER"),
    password: System.get_env("DATABASE_PASS"),
    database: System.get_env("DATABASE_NAME"),
    hostname: System.get_env("DATABASE_HOST")
end

# See [the detailed docs](https://hexdocs.pm/pigeon/apns-apple-ios.html) for setting up APNS.
config :charisma, :apns_topic, System.get_env("APNS_TOPIC")

config :pigeon, :apns,
  apns_default: %{
    key: System.get_env("APNS_KEY"),
    key_identifier: System.get_env("APNS_KEY_IDENTIFIER"),
    team_id: System.get_env("APNS_TEAM_ID"),
    mode: String.to_atom(System.get_env("APNS_MODE") || "prod")
  }

config :pigeon, :fcm,
  fcm_default: %{
    key: System.get_env("FCM_KEY")
  }
