use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :charisma, CharismaWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :charisma, Charisma.Repo,
  username: System.get_env("DATABASE_USER") || "postgres",
  password: System.get_env("DATABASE_PASS") || "postgres",
  database: "charisma_test",
  hostname: System.get_env("DATABASE_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :tesla, adapter: Tesla.Mock

config :web_push_encryption, :vapid_details,
  subject: "mailto:administrator@example.com",
  public_key:
    "BA-DazhzrCn2t5mgcYR27ux9Q8Xdr2kFJxjaBKoLU9pGlelH7wyJlwUhkGlLOR20LTiEu5ATOiBCjub0Od5nZ-Q",
  private_key: "G0Nmzvn-Tpa8aEtoU1wFgKgZRYt3Ha9ODdJw1wQj6-U"

# config :pleroma_job_queue, disabled: true

config :pigeon, http2_client: Pigeon.Http2.Client.Mock

config :pigeon, :apns,
  apns_default: %{
    cert: {:charisma, "keys/test.key"},
    key: {:charisma, "keys/test.key.pub"},
    mode: :dev
  }
