defmodule Charisma.Pleroma do
  @moduledoc """
  Pleroma's API Client
  """

  @subscription_path "/api/v1/push/subscription"

  def new(host, token) do
    middleware = [
      Tesla.Middleware.Logger,
      {Tesla.Middleware.BaseUrl, host},
      Tesla.Middleware.JSON,
      {Tesla.Middleware.Headers, [{"authorization", "Bearer: " <> token}]}
    ]

    Tesla.client(middleware)
  end

  def get_subscription(client) do
    client
    |> Tesla.get(@subscription_path)
    |> process_response()
  end

  def create_push_subscription(client, subscription) do
    client
    |> Tesla.post(@subscription_path, subscription)
    |> process_response()
  end

  def update_push_subscription(client, subscription) do
    client
    |> Tesla.put(@subscription_path, subscription)
    |> process_response()
  end

  def delete_push_subscription(client) do
    client
    |> Tesla.delete(@subscription_path)
    |> process_response()
  end

  defp process_response({:ok, %{status: status} = response}) when status >= 400 do
    case response do
      %{body: %{"error" => err}} -> {:error, {status, err}}
      %{body: body} -> {:error, {status, body}}
    end
  end

  defp process_response({:ok, response}) do
    case response do
      %{body: %{"error" => err}} -> {:error, err}
      %{body: body} -> {:ok, body}
    end
  end

  defp process_response(err), do: err
end
