defmodule Charisma.Push do
  @moduledoc """
  The Push context.
  """
  require Logger

  import Ecto.Query, warn: false

  alias Charisma.Pleroma
  alias Charisma.Push.Subscription
  alias Charisma.Repo
  alias Ecto.Changeset

  @doc """
  Gets a single subscription.

  Raises `Ecto.NoResultsError` if the Subscription does not exist.

  ## Examples

      iex> get_subscription!(123)
      %Subscription{}

      iex> get_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  def get_subscription!(id), do: Repo.get!(Subscription, id)

  def get_subscription_by_url_id!(url_id) do
    {device_id, host} = decode_url_id!(url_id)

    Repo.get_by!(Subscription, device_id: device_id, host: host)
  end

  @doc """
  Creates a subscription.

  ## Examples

      iex> create_subscription(token, %{field: value})
      {:ok, %Subscription{}}

      iex> create_subscription(token, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_subscription(token, attrs) do
    {public, private} = :crypto.generate_key(:ecdh, :prime256v1)

    changeset =
      Subscription.changeset(
        %Subscription{
          web_push_key_auth: :crypto.strong_rand_bytes(16) |> Base.url_encode64(padding: false),
          web_push_key_p256dh: Base.url_encode64(public, padding: false),
          web_push_key_private: Base.url_encode64(private, padding: false)
        },
        attrs
      )

    insert_opts = [on_conflict: :replace_all, conflict_target: [:device_id, :host]]

    with {:ok, changeset} <- validate_changeset(changeset),
         {:ok, %{"id" => _}} <- create_pleroma_push_subscription(token, changeset) do
      Repo.insert(changeset, insert_opts)
    end
  end

  defp create_pleroma_push_subscription(token, %Changeset{} = changeset) do
    subscription = Changeset.apply_changes(changeset)
    web_push_subscription = make_web_push_subscription(subscription)

    subscription.host
    |> Pleroma.new(token)
    |> Pleroma.create_push_subscription(web_push_subscription)
  end

  defp make_web_push_subscription(subscription) do
    endpoint =
      CharismaWeb.Router.Helpers.web_push_url(
        CharismaWeb.Endpoint,
        :create,
        subscription_url_id(subscription)
      )

    %{
      subscription: %{
        endpoint: endpoint,
        keys: %{
          auth: subscription.web_push_key_auth,
          p256dh: subscription.web_push_key_p256dh
        }
      },
      data: subscription.data
    }
  end

  @doc """
  Updates a subscription.

  ## Examples

      iex> update_subscription(token, subscription, %{field: new_value})
      {:ok, %Subscription{}}

      iex> update_subscription(token, subscription, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_subscription(token, %Subscription{} = subscription, attrs) do
    changeset = Subscription.changeset(subscription, attrs)

    with {:ok, changeset} <- validate_changeset(changeset),
         {:ok, %{"id" => _}} <- update_pleroma_push_subscription(token, changeset) do
      Repo.update(changeset)
    end
  end

  defp validate_changeset(changeset) do
    case changeset.valid? do
      true -> {:ok, changeset}
      false -> {:error, changeset}
    end
  end

  defp update_pleroma_push_subscription(token, %Changeset{} = changeset) do
    subscription = Changeset.apply_changes(changeset)

    subscription.host
    |> Pleroma.new(token)
    |> Pleroma.update_push_subscription(%{data: subscription.data})
  end

  @doc """
  Deletes a Subscription.

  ## Examples

      iex> delete_subscription(token, subscription)
      {:ok, %Subscription{}}

      iex> delete_subscription(token, subscription)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscription(token, %Subscription{} = subscription) do
    with {:ok, _} <- subscription.host |> Pleroma.new(token) |> Pleroma.delete_push_subscription() do
      Repo.delete(subscription)
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subscription changes.

  ## Examples

      iex> change_subscription(subscription)
      %Ecto.Changeset{source: %Subscription{}}

  """
  def change_subscription(%Subscription{} = subscription) do
    Subscription.changeset(subscription, %{})
  end

  def send(%{platform: platform} = subscription, params) when platform in ["apns", "fcm"] do
    notifiction = create_push_notifiction(subscription, params)

    platform
    |> String.to_atom()
    |> PleromaJobQueue.enqueue(Charisma.Push.Worker, [notifiction])
  end

  def send(%{platform: platform} = subscription, _params) do
    Logger.error(
      "Failed to send a push. Platform `#{platform}` is not supported. Subscription ID: #{
        subscription.id
      }"
    )

    {:error, "Unsupported platform"}
  end

  def subscription_url_id(%{device_id: device_id, host: host}) do
    Base.url_encode64(device_id <> "|" <> host, padding: false)
  end

  def decode_url_id!(url_id) do
    [device_id, host] =
      url_id
      |> Base.url_decode64!(padding: false)
      |> String.split("|")

    {device_id, host}
  end

  def create_push_notifiction(%{platform: "apns"} = subscription, data) do
    alias Pigeon.APNS.Notification, as: N

    topic = Application.get_env(:charisma, :apns_topic)

    {alert, custom} = Map.split(data, ["title", "body"])
    custom = Map.put(custom, "host", subscription.host)

    alert
    |> N.new(subscription.device_id, topic)
    |> N.put_custom(custom)
  end

  def create_push_notifiction(%{platform: "fcm"} = subscription, %{"body" => body} = data) do
    alias Pigeon.FCM.Notification, as: N

    subscription.device_id
    |> N.new()
    |> N.put_notification(%{"body" => body})
    |> N.put_data(data)
  end
end
