defmodule Charisma.Push.Subscription do
  @moduledoc """
  Push subscription schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "push_subscriptions" do
    field :device_id, :string
    field :host, :string
    field :platform, :string
    field :web_push_key_auth, :string
    field :web_push_key_p256dh, :string
    field :web_push_key_private, :string
    field :data, :map, default: %{}

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [
      :device_id,
      :platform,
      :host,
      :web_push_key_auth,
      :web_push_key_p256dh,
      :web_push_key_private,
      :data
    ])
    |> validate_required([
      :device_id,
      :platform,
      :host,
      :web_push_key_auth,
      :web_push_key_p256dh,
      :web_push_key_private,
      :data
    ])
    |> unique_constraint(:device_id,
      name: :push_subscriptions_device_id_host_index,
      message: "Subscription for `#{attrs["host"]}` already exist"
    )
  end
end
