defmodule Charisma.Push.Worker do
  @moduledoc """
  Sends native pushes to mobile devices
  """
  require Logger
  alias Pigeon.APNS
  alias Pigeon.FCM

  def perform(%APNS.Notification{} = notification) do
    started_at = :erlang.timestamp()

    notification
    |> APNS.push()
    |> process_result(started_at)
  end

  def perform(%FCM.Notification{} = notification) do
    started_at = :erlang.timestamp()

    notification
    |> FCM.push()
    |> process_result(started_at)
  end

  def process_result(n, started_at) do
    time = :timer.now_diff(:erlang.timestamp(), started_at)

    :telemetry.execute([:push, :done], %{duration: time}, %{platform: platform(n)})

    Logger.debug("Push result (in #{time} μs): #{inspect(n, pretty: true)}")
  end

  defp platform(%APNS.Notification{}), do: :apns
  defp platform(%FCM.Notification{}), do: :fcm
end
