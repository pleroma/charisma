defmodule Charisma.Repo do
  use Ecto.Repo,
    otp_app: :charisma,
    adapter: Ecto.Adapters.Postgres
end
