defmodule Charisma.WebPush do
  @moduledoc """
  WebPush decryption
  """

  @key_length 16
  @tag_length 16
  @nonce_length 12
  @sha256_length 32

  @one_buffer <<1>>
  @auth_info "Content-Encoding: auth" <> <<0>>

  def decrypt!(data, params) do
    validate_length!(params.client_auth_token, 16, "Client auth token is not 16 bytes")
    validate_length!(params.salt, 16, "Salt must be 16-bytes long")
    validate_length!(params.client_private_key, 32, "Client private key is invalid")

    decrypt_payload(data, params)
  end

  defp decrypt_payload(data, params) do
    {content_decryption_key, nonce} = extract_key(params)
    size = byte_size(data)
    cipher_text = binary_part(data, 0, size - @tag_length)
    cipher_tag = binary_part(data, size, -@tag_length)

    <<0, 0>> <> payload =
      :crypto.block_decrypt(
        :aes_gcm,
        content_decryption_key,
        nonce,
        {"", cipher_text, cipher_tag}
      )

    payload
  end

  def extract_key(params) do
    context = create_context(params.client_public_key, params.server_public_key)

    shared_secret =
      :crypto.compute_key(
        :ecdh,
        params.server_public_key,
        params.client_private_key,
        :prime256v1
      )

    prk = hkdf(params.client_auth_token, shared_secret, @auth_info, @sha256_length)

    content_encryption_key = hkdf(params.salt, prk, create_info("aesgcm", context), @key_length)

    nonce = hkdf(params.salt, prk, create_info("nonce", context), @nonce_length)

    {content_encryption_key, nonce}
  end

  defp create_context(client_public_key, _server_public_key)
       when byte_size(client_public_key) != 65,
       do: raise(ArgumentError, "invalid client public key length")

  defp create_context(_client_public_key, server_public_key)
       when byte_size(server_public_key) != 65,
       do: raise(ArgumentError, "invalid server public key length")

  defp create_context(client_public_key, server_public_key) do
    <<0, byte_size(client_public_key)::unsigned-big-integer-size(16)>> <>
      client_public_key <>
      <<byte_size(server_public_key)::unsigned-big-integer-size(16)>> <> server_public_key
  end

  def create_info(_type, context) when byte_size(context) != 135,
    do: raise(ArgumentError, "Context argument has invalid size")

  def create_info(type, context) do
    "Content-Encoding: " <> type <> <<0>> <> "P-256" <> context
  end

  defp hkdf(salt, ikm, info, length) do
    :sha256
    |> :crypto.hmac_init(prk(salt, ikm))
    |> :crypto.hmac_update(info)
    |> :crypto.hmac_update(@one_buffer)
    |> :crypto.hmac_final()
    |> :binary.part(0, length)
  end

  defp prk(salt, ikm) do
    :sha256
    |> :crypto.hmac_init(salt)
    |> :crypto.hmac_update(ikm)
    |> :crypto.hmac_final()
  end

  defp validate_length!(bytes, expected_size, _message) when byte_size(bytes) == expected_size,
    do: :ok

  defp validate_length!(_bytes, _expected_size, message) do
    raise ArgumentError, message
  end
end
