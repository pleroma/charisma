defmodule CharismaWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use CharismaWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(CharismaWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(CharismaWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, {:error, {status_code, error_message}}) do
    conn
    |> put_status(status_code)
    |> json(%{error: error_message})
  end

  def call(conn, {:error, error_message}) do
    conn
    |> put_status(:internal_server_error)
    |> json(%{error: error_message})
  end
end
