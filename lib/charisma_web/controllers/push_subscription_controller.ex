defmodule CharismaWeb.PushSubscriptionController do
  use CharismaWeb, :controller

  alias Charisma.Pleroma
  alias Charisma.Push
  alias Charisma.Push.Subscription

  action_fallback CharismaWeb.FallbackController

  plug :require_token
  plug :load_push_subscription when action != :create

  def create(%{assigns: %{token: token}} = conn, subscription_params) do
    with {:ok, %Subscription{} = subscription} <-
           Push.create_subscription(token, subscription_params) do
      conn
      |> put_status(:created)
      |> put_resp_header(
        "location",
        push_subscription_path(conn, :show, subscription)
      )
      |> render("show.json", push_subscription: subscription)
    end
  end

  def show(%{assigns: %{push_subscription: subscription, token: token}} = conn, _) do
    with {:ok, _} <- subscription.host |> Pleroma.new(token) |> Pleroma.get_subscription() do
      render(conn, "show.json")
    end
  end

  def update(
        %{assigns: %{push_subscription: subscription, token: token}} = conn,
        %{"data" => data}
      ) do
    with {:ok, %Subscription{} = subscription} <-
           Push.update_subscription(token, subscription, %{"data" => data}) do
      render(conn, "show.json", push_subscription: subscription)
    end
  end

  def delete(%{assigns: %{push_subscription: subscription, token: token}} = conn, _) do
    with {:ok, %Subscription{}} <- Push.delete_subscription(token, subscription) do
      send_resp(conn, :no_content, "")
    end
  end

  defp push_subscription_path(conn, method, subscription) do
    url_id = Push.subscription_url_id(subscription)
    Routes.push_subscription_path(conn, method, url_id)
  end
end
