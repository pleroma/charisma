defmodule CharismaWeb.WebPushController do
  use CharismaWeb, :controller

  alias Charisma.Push
  alias Charisma.WebPush

  plug :validate_content_encoding
  plug :parse_keys
  plug :parse_body
  plug :load_push_subscription

  def create(
        %{
          assigns: %{
            push_subscription: push_subscription,
            encrypted_payload: encrypted_payload,
            keys: keys
          }
        } = conn,
        _params
      ) do
    payload =
      encrypted_payload
      |> WebPush.decrypt!(%{
        client_public_key: ub64!(push_subscription.web_push_key_p256dh),
        client_private_key: ub64!(push_subscription.web_push_key_private),
        client_auth_token: ub64!(push_subscription.web_push_key_auth),
        server_public_key: keys.dh,
        salt: keys.salt
      })
      |> Jason.decode!()

    Push.send(push_subscription, payload)

    send_resp(conn, :no_content, "")
  end

  defp ub64!(value), do: Base.url_decode64!(value, padding: false)

  defp parse_keys(conn, _) do
    with {:salt, ["salt=" <> salt]} <- {:salt, get_req_header(conn, "encryption")},
         {:salt, {:ok, salt}} <- {:salt, Base.url_decode64(salt, padding: false)},
         {:keys, [keys]} <- {:keys, get_req_header(conn, "crypto-key")} do
      parsed_keys =
        keys
        |> String.split(";")
        |> Enum.map(fn str ->
          [name, value] = String.split(str, "=")
          {String.to_atom(name), ub64!(value)}
        end)
        |> Enum.into(%{})
        |> Map.put(:salt, salt)

      assign(conn, :keys, parsed_keys)
    else
      _ ->
        conn
        |> put_status(:unprocessable_entity)
        |> json(%{error: "Invalid Headers"})
        |> halt()
    end
  end

  defp validate_content_encoding(conn, _) do
    case get_req_header(conn, "content-encoding") do
      ["aesgcm"] ->
        conn

      _ ->
        conn
        |> put_status(:not_implemented)
        |> json(%{error: "Bad content encoding"})
        |> halt()
    end
  end

  defp parse_body(conn, _) do
    case Plug.Conn.read_body(conn, length: 5000) do
      {:ok, body, conn} ->
        assign(conn, :encrypted_payload, body)

      _ ->
        conn
        |> put_status(:unprocessable_entity)
        |> json(%{error: "Payload is too large"})
        |> halt()
    end
  end
end
