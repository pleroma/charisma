defmodule CharismaWeb.Plugs do
  @moduledoc """
  Plug middleware functions
  """
  import Plug.Conn
  import Phoenix.Controller, only: [json: 2]

  def load_push_subscription(%{params: %{"url_id" => url_id}} = conn, _) do
    assign(conn, :push_subscription, Charisma.Push.get_subscription_by_url_id!(url_id))
  end

  def require_token(%{assigns: %{token: token}} = conn, _) when is_binary(token), do: conn

  def require_token(conn, _) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Token is required"})
    |> halt()
  end
end
