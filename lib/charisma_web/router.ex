defmodule CharismaWeb.Router do
  use CharismaWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :parse_token
  end

  scope "/api/v1", CharismaWeb do
    pipe_through :api

    post "/push/:url_id", WebPushController, :create

    resources "/push_subscriptions", PushSubscriptionController,
      only: [:create, :show, :update, :delete],
      param: "url_id"
  end

  defp parse_token(conn, _) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] -> assign(conn, :token, token)
      _ -> conn
    end
  end
end
