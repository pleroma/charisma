defmodule CharismaWeb.PushSubscriptionView do
  use CharismaWeb, :view
  alias CharismaWeb.PushSubscriptionView

  def render("show.json", %{push_subscription: subscription}) do
    render_one(subscription, PushSubscriptionView, "push_subscription.json")
  end

  def render("push_subscription.json", %{push_subscription: subscription}) do
    %{
      id: Charisma.Push.subscription_url_id(subscription),
      device_id: subscription.device_id,
      platform: subscription.platform,
      host: subscription.host,
      data: subscription.data
    }
  end
end
