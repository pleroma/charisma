defmodule Charisma.Repo.Migrations.CreatePushSubscriptions do
  use Ecto.Migration

  def change do
    create table(:push_subscriptions) do
      add :device_id, :string, null: false
      add :platform, :string, null: false
      add :host, :string, null: false
      add :web_push_key_auth, :string, null: false
      add :web_push_key_p256dh, :string, null: false
      add :web_push_key_private, :string, null: false
      add :data, :map, null: false

      timestamps()
    end

    create unique_index(:push_subscriptions, [:device_id, :host])
  end
end
