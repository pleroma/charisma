defmodule Charisma.PleromaTest do
  use ExUnit.Case
  import Tesla.Mock

  alias Charisma.Pleroma

  @tag capture_log: true
  test "process_response/1" do
    mock(fn
      %{method: :get} -> "error"
    end)

    client = Pleroma.new("xxx", "xxx")
    assert {:error, "error"} == Pleroma.get_subscription(client)
  end
end
