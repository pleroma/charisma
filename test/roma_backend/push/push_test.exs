defmodule Charisma.PushTest do
  use Charisma.DataCase, async: true

  import Tesla.Mock

  alias Charisma.Push

  setup do
    mock(fn
      %{method: :post} -> json(%{"id" => "123"})
      %{method: :put} -> json(%{"id" => "123"})
      %{method: :delete} -> json(%{})
    end)

    :ok
  end

  describe "push_subscriptions" do
    alias Charisma.Push.Subscription

    @token "xxxx"

    @valid_attrs %{
      device_id: "some device_id",
      host: "https://pleroma_host.com",
      platform: "apns",
      web_push_key_auth: "some web_push_key_auth",
      web_push_key_p256dh: "some web_push_key_p256dh",
      web_push_key_private: "some web_push_key_private"
    }
    @update_attrs %{
      device_id: "some updated device_id",
      host: "some updated host",
      web_push_key_auth: "some updated web_push_key_auth",
      web_push_key_p256dh: "some updated web_push_key_p256dh",
      web_push_key_private: "some updated web_push_key_private"
    }
    @invalid_attrs %{
      device_id: nil,
      host: nil,
      platform: nil,
      web_push_key_auth: nil,
      web_push_key_p256dh: nil,
      web_push_key_private: nil
    }

    def subscription_fixture(attrs \\ %{}) do
      {:ok, subscription} = Push.create_subscription(@token, Enum.into(attrs, @valid_attrs))

      subscription
    end

    test "get_subscription!/1 returns the subscription with given id" do
      subscription = subscription_fixture()
      assert Push.get_subscription!(subscription.id) == subscription
    end

    test "create_subscription/2 with valid data creates a subscription" do
      assert {:ok, %Subscription{} = subscription} =
               Push.create_subscription(@token, @valid_attrs)

      assert subscription.device_id == "some device_id"
      assert subscription.host == "https://pleroma_host.com"
      assert subscription.platform == "apns"
      assert subscription.web_push_key_auth == "some web_push_key_auth"
      assert subscription.web_push_key_p256dh == "some web_push_key_p256dh"
      assert subscription.web_push_key_private == "some web_push_key_private"
    end

    @tag capture_log: true

    test "create_subscription/2 doesn't create subscription if subscription request to Pleroma failed" do
      mock(fn
        %{method: :post} -> %Tesla.Env{status: 500}
      end)

      assert {:error, _} = Push.create_subscription(@token, @valid_attrs)
    end

    test "create_subscription/2 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{} = changeset} =
               Push.create_subscription(@token, @invalid_attrs)

      assert %{
               device_id: ["can't be blank"],
               host: ["can't be blank"],
               platform: ["can't be blank"],
               web_push_key_auth: ["can't be blank"],
               web_push_key_p256dh: ["can't be blank"],
               web_push_key_private: ["can't be blank"]
             } == errors_on(changeset)
    end

    test "update_subscription/2 with valid data updates the subscription" do
      subscription = subscription_fixture()

      assert {:ok, %Subscription{} = subscription} =
               Push.update_subscription(@token, subscription, @update_attrs)
    end

    test "update_subscription/2 with invalid data returns error changeset" do
      subscription = subscription_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Push.update_subscription(@token, subscription, @invalid_attrs)

      assert subscription == Push.get_subscription!(subscription.id)
    end

    test "delete_subscription/1 deletes the subscription" do
      subscription = subscription_fixture()
      assert {:ok, %Subscription{}} = Push.delete_subscription(@token, subscription)
      assert_raise Ecto.NoResultsError, fn -> Push.get_subscription!(subscription.id) end
    end

    test "change_subscription/1 returns a subscription changeset" do
      subscription = subscription_fixture()
      assert %Ecto.Changeset{} = Push.change_subscription(subscription)
    end

    test "send/2 sends an APNS push notification" do
      {:ok, %Subscription{} = subscription} = Push.create_subscription(@token, @valid_attrs)

      assert :ok == Push.send(subscription, %{})
    end

    test "send/2 sends a FCM push notification" do
      {:ok, %Subscription{} = subscription} =
        Push.create_subscription(@token, %{@valid_attrs | platform: "fcm"})

      assert :ok == Push.send(subscription, %{"body" => "test"})
    end

    @tag capture_log: true
    test "send/2 returns an error if the platform is unsupported" do
      {:ok, %Subscription{} = subscription} =
        Push.create_subscription(@token, %{@valid_attrs | platform: "foobar"})

      assert {:error, "Unsupported platform"} == Push.send(subscription, %{})
    end
  end
end
