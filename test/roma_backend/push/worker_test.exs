defmodule Charisma.Push.WorkerTest do
  use ExUnit.Case

  alias Charisma.Push.Worker

  test "process_result/2" do
    assert :ok == Worker.process_result(%Pigeon.APNS.Notification{}, :erlang.timestamp())
  end
end
