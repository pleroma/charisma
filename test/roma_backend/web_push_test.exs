defmodule Charisma.WebPushTest do
  use ExUnit.Case

  alias Charisma.WebPush

  @bytes16 <<0::size(128)>>
  @bytes32 <<0::size(256)>>
  @bytes65 <<0::size(520)>>

  test "validate sizes" do
    assert_raise ArgumentError, "Client auth token is not 16 bytes", fn ->
      WebPush.decrypt!("", %{
        client_auth_token: "",
        salt: "",
        client_private_key: ""
      })
    end

    assert_raise ArgumentError, "Salt must be 16-bytes long", fn ->
      WebPush.decrypt!("", %{
        client_auth_token: @bytes16,
        salt: "",
        client_private_key: ""
      })
    end

    assert_raise ArgumentError, "Client private key is invalid", fn ->
      WebPush.decrypt!("", %{
        client_auth_token: @bytes16,
        salt: @bytes16,
        client_private_key: ""
      })
    end

    assert_raise ArgumentError, "invalid client public key length", fn ->
      WebPush.decrypt!("", %{
        client_auth_token: @bytes16,
        salt: @bytes16,
        client_private_key: @bytes32,
        client_public_key: "",
        server_public_key: ""
      })
    end

    assert_raise ArgumentError, "invalid server public key length", fn ->
      WebPush.decrypt!("", %{
        client_auth_token: @bytes16,
        salt: @bytes16,
        client_private_key: @bytes32,
        client_public_key: @bytes65,
        server_public_key: ""
      })
    end

    assert_raise ArgumentError, "Context argument has invalid size", fn ->
      WebPush.create_info("", "")
    end
  end
end
