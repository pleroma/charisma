defmodule CharismaWeb.PushSubscriptionControllerTest do
  use CharismaWeb.ConnCase

  import Tesla.Mock

  alias Charisma.Push

  @token "xxxx"

  @create_attrs %{
    device_id: "some device_id",
    host: "https://pleroma_host.com",
    platform: "some platform",
    web_push_key_auth: "some web_push_key_auth",
    web_push_key_p256dh: "some web_push_key_p256dh",
    web_push_key_private: "some web_push_key_private",
    data: %{
      follow: true,
      favourite: true,
      reblog: true,
      mention: true
    }
  }
  @update_attrs %{
    data: %{
      follow: false,
      favourite: true,
      reblog: false,
      mention: true
    }
  }
  @invalid_attrs %{
    device_id: nil,
    host: nil,
    platform: nil,
    web_push_key_auth: nil,
    web_push_key_p256dh: nil,
    web_push_key_private: nil,
    data: nil
  }

  def fixture(:subscription) do
    {:ok, subscription} = Push.create_subscription(@token, @create_attrs)
    subscription
  end

  setup %{conn: conn} do
    mock(fn
      %{method: :post} -> json(%{"id" => "123"})
      %{method: :put} -> json(%{"id" => "123"})
      %{method: :get} -> json(%{"id" => "123"})
      %{method: :delete} -> json(%{})
    end)

    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer " <> @token)

    {:ok, conn: conn}
  end

  describe "create subscription" do
    test "renders subscription when data is valid", %{conn: conn} do
      conn = post(conn, Routes.push_subscription_path(conn, :create), @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)

      conn = get(conn, Routes.push_subscription_path(conn, :show, id))

      assert %{
               "id" => id,
               "device_id" => "some device_id",
               "host" => "https://pleroma_host.com",
               "platform" => "some platform"
             } = json_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.push_subscription_path(conn, :create), @invalid_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end

    test "requires authorization", %{conn: conn} do
      conn =
        conn
        |> delete_req_header("authorization")
        |> post(Routes.push_subscription_path(conn, :create), @create_attrs)

      assert json_response(conn, :unauthorized) == %{"error" => "Token is required"}
    end
  end

  describe "update subscription" do
    setup [:create_subscription]

    test "renders subscription when data is valid", %{
      conn: conn,
      url_id: url_id
    } do
      conn = put(conn, Routes.push_subscription_path(conn, :update, url_id), @update_attrs)
      assert %{"id" => ^url_id} = json_response(conn, 200)

      conn = get(conn, Routes.push_subscription_path(conn, :show, url_id))

      assert %{
               "id" => url_id,
               "data" => %{
                 "follow" => false,
                 "favourite" => true,
                 "reblog" => false,
                 "mention" => true
               }
             } = json_response(conn, 200)
    end

    test "renders errors when data is invalid", %{
      conn: conn,
      url_id: url_id
    } do
      conn = put(conn, Routes.push_subscription_path(conn, :update, url_id), @invalid_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete subscription" do
    setup [:create_subscription]

    test "deletes chosen subscription", %{conn: conn, url_id: url_id} do
      conn = delete(conn, Routes.push_subscription_path(conn, :delete, url_id))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.push_subscription_path(conn, :show, url_id))
      end
    end
  end

  defp create_subscription(_) do
    subscription = fixture(:subscription)
    url_id = Push.subscription_url_id(subscription)
    {:ok, subscription: subscription, url_id: url_id}
  end
end
