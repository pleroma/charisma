defmodule CharismaWeb.WebPushControllerTest do
  use CharismaWeb.ConnCase, async: true

  import Tesla.Mock

  alias Charisma.Push

  @token "xxxx"

  @create_attrs %{
    device_id: "some device_id",
    host: "https://pleroma_host.com",
    platform: "apns",
    data: %{
      follow: true,
      favourite: true,
      reblog: true,
      mention: true
    }
  }

  def fixture(:subscription) do
    {:ok, subscription} = Push.create_subscription(@token, @create_attrs)
    subscription
  end

  setup %{conn: conn} do
    mock(fn %{method: :post} -> json(%{"id" => "123"}) end)

    {:ok, conn: conn}
  end

  setup [:create_subscription]

  test "create/2", %{conn: conn, url_id: url_id, subscription: subscription} do
    Application.put_env(:charisma, :testing_web_push_pid, self())

    endpoint = Routes.web_push_url(CharismaWeb.Endpoint, :create, url_id)

    body = ~s({"hello": "elixir"})

    web_push_subscription = %{
      keys: %{p256dh: subscription.web_push_key_p256dh, auth: subscription.web_push_key_auth},
      endpoint: endpoint
    }

    {headers, body} = make_web_push(body, web_push_subscription)

    conn =
      headers
      |> Enum.reduce(conn, fn {header, val}, conn -> put_req_header(conn, header, val) end)
      |> post(Routes.web_push_path(conn, :create, url_id), body)

    assert conn.status == 204

    assert_receive {:push,
                    %{
                      "aps" => %{"alert" => %{}},
                      "hello" => "elixir",
                      "host" => "https://pleroma_host.com"
                    }}
  end

  test "validate headers", %{conn: conn, url_id: url_id} do
    conn =
      conn
      |> put_req_header("content-type", "application/octet-stream")
      |> put_req_header("content-encoding", "aesgcm")
      |> post(Routes.web_push_path(conn, :create, url_id), "")

    assert %{"error" => "Invalid Headers"} == json_response(conn, 422)
  end

  test "validate content encoding", %{conn: conn, url_id: url_id} do
    conn =
      conn
      |> put_req_header("content-type", "application/octet-stream")
      |> post(Routes.web_push_path(conn, :create, url_id), "")

    assert %{"error" => "Bad content encoding"} == json_response(conn, 501)
  end

  test "validate body length", %{conn: conn, url_id: url_id, subscription: subscription} do
    len = 8 * 11 * 1024
    body = <<0::size(len)>>

    endpoint = Routes.web_push_url(CharismaWeb.Endpoint, :create, url_id)

    web_push_subscription = %{
      keys: %{p256dh: subscription.web_push_key_p256dh, auth: subscription.web_push_key_auth},
      endpoint: endpoint
    }

    {headers, _body} = make_web_push("test", web_push_subscription)

    conn =
      headers
      |> Enum.reduce(conn, fn {header, val}, conn -> put_req_header(conn, header, val) end)
      |> post(Routes.web_push_path(conn, :create, url_id), body)

    assert %{"error" => "Payload is too large"} == json_response(conn, 422)
  end

  defp make_web_push(message, %{endpoint: endpoint} = subscription) do
    payload = WebPushEncryption.Encrypt.encrypt(message, subscription)

    headers =
      WebPushEncryption.Vapid.get_headers(make_audience(endpoint), "aesgcm")
      |> Map.merge(%{
        "TTL" => "0",
        "Content-Encoding" => "aesgcm",
        "content-type" => "application/octet-stream",
        "Encryption" => "salt=#{ub64(payload.salt)}"
      })

    headers =
      headers
      |> Map.put("Crypto-Key", "dh=#{ub64(payload.server_public_key)};" <> headers["Crypto-Key"])
      |> Enum.into([])
      |> Enum.map(fn {header, value} -> {String.downcase(header), value} end)

    {headers, payload.ciphertext}
  end

  defp ub64(value) do
    Base.url_encode64(value, padding: false)
  end

  defp make_audience(endpoint) do
    parsed = URI.parse(endpoint)
    parsed.scheme <> "://" <> parsed.host
  end

  defp create_subscription(_) do
    subscription = fixture(:subscription)
    url_id = Push.subscription_url_id(subscription)
    {:ok, subscription: subscription, url_id: url_id}
  end
end
