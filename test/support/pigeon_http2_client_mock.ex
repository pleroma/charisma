defmodule Pigeon.Http2.Client.Mock do
  @moduledoc false

  @behaviour Pigeon.Http2.Client

  def start do
    {:ok, :ok}
  end

  def connect(_uri, _scheme, _opts) do
    {:ok, self()}
  end

  def send_request(_pid, _headers, data) do
    :charisma
    |> Application.get_env(:testing_web_push_pid, self())
    |> send({:push, Jason.decode!(data)})

    :ok
  end

  def send_ping(_pid), do: :ok
  def handle_end_stream(_, _state), do: :ok
end
